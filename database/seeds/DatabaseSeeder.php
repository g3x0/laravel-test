<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        User::truncate();

        User::create(['email' => 'user@yahoo.com', 'name' => 'askwrite', 'password' => '$2y$10$C6gQ75d.AtURk8t7gPd8bO1bFkWGyJBPsAZtBfOTlW.H8MrfLZbvG']);
    }

}
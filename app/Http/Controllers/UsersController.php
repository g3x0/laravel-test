<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return \View::make("users.index")->with("users", $users);
    }

    public function deleteU($id)
    {
        $user = User::where("id", $id)->get();
        if(count($user)) {
            User::where("id", $id)->delete();
        } else {
            return "Error";
        }
    }

    public function reg() {
        return \View::make("auth/register");
    }

    public function regP() {
        $validator = Validator::make(Input::all(), array(
                        'username' => 'required', 
                        'email' => 'required|email|unique:users', 
                        'password' => 'required', 
                        'password_confirmation' => 'same:password',
                    )
                );

        if($validator->fails()) {
            return Redirect::to("/auth/reg")->withErrors($validator)->withInput();
        } else {
            $email = htmlentities(Input::get("email"), ENT_QUOTES);
            $name = htmlentities(Input::get("username"), ENT_QUOTES);
            $pass = Hash::make(Input::get("password"));
            $user = new User;
            $user->email = $email;
            $user->name = $name;
            $user->password = $pass;
            $user->save();
            return Redirect::to("/auth/login")->with('message', 'User inregistrat cu succes!');;
        }
    }

    public function editU($id) {
        $user = User::where("id", $id)->get();
        if(count($user)) {
            return \View::make("users.edit")->with("user", $user);
        }
        return Redirect::route("home");
    }

    public function editUp($id) {
        $userI = DB::table('users')->where("id", $id)->get();
        if(!empty($userI)) {
            $validator = Validator::make(Input::all(),
                array(
                    "NewPassword" => "min:6",
                    "RepeatNewPassword" => "same:NewPassword"
                )
            );
            if($validator->fails()) {
                return Redirect::to("/utilizatori/edit/".$id)->withErrors($validator)->withInput();
            } else {
                $username = htmlentities(Input::get("Nume"), ENT_QUOTES);
                $password = htmlentities(Input::get("NewPassword"), ENT_QUOTES);
                if($password == "") {
                    $passhash = $userI[0]->password;
                } else {
                    $passhash = Hash::make($password);
                }
                $user = User::find($id);
                $user->password = $passhash;
                if($username != $userI[0]->name) {
                    $user->name = $username;
                }
                $user->save();
                return Redirect::to("/utilizatori/edit/".$id)->with('message', 'Schimbari efectuate cu success!');
            }
        } else {
            return Redirect::route('home');
        }
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'HomeController@home', 'as' => 'home' ]);

Route::get('/home', ['uses' => 'HomeController@home']);

Route::get('/auth/reg', ['uses' => 'UsersController@reg', 'as' => 'register']);

Route::post('/auth/reg', ['uses' => 'UsersController@regP']);

Route::get('/utilizatori', ['uses' => 'UsersController@index', 'as' => 'users', 'middleware' => 'auth']);

Route::get('/utilizatori/edit/{id}', ['uses' => 'UsersController@editU', 'middleware' => 'auth']);

Route::post('/utilizatori/edit/{id}', ['uses' => 'UsersController@editUp', 'middleware' => 'auth']);

Route::get('/utilizatori/delete/{id}', ['uses' => 'UsersController@deleteU', 'middleware' => 'auth']);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
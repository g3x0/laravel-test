@extends("layout.main")
@section("title") Login @stop
@section("navli")
	<li class="active"><a href="/auth/login">Login</a></li>
	<li><a href="/auth/reg">Register</a></li>
@stop
@section("paneltitle") Login @stop

@section("content")
	<form action="" method="POST" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="inputUsername" class="col-sm-2 control-label">Email:</label>
			<div class="col-sm-10">
				<input type="text" name="email" id="inputUsername" class="form-control" value="{{ htmlentities(Input::old("username"), ENT_QUOTES) }}" title="">
				@if($errors->has("email"))
				{{ $errors->first("email") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-sm-2 control-label">Password:</label>
			<div class="col-sm-10">
				<input type="password" name="password" id="inputPassword" class="form-control" value="" title="">
				@if($errors->has("password"))
				{{ $errors->first("password") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
			<center>{{ Session::get('message') }}</center><br>
			<center><button type="submit" class="btn btn-primary">Login</button></center>
		</div>
	</form>
@stop
@extends("layout.main")
@section("title") Register @stop
@section("navli")
	<li><a href="/auth/login">Login</a></li>
	<li class="active"><a href="/auth/register">Register</a></li>
@stop
@section("paneltitle") Register @stop

@section("content")
	<form action="" method="POST" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="inputUsername" class="col-sm-2 control-label">Name:</label>
			<div class="col-sm-10">
				<input type="text" name="username" id="inputUsername" class="form-control" value="{{ htmlentities(Input::old("username"), ENT_QUOTES) }}" title="">
				@if($errors->has("username"))
				{{ $errors->first("username") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputUsername" class="col-sm-2 control-label">Email:</label>
			<div class="col-sm-10">
				<input type="email" name="email" id="inputUsername" class="form-control" value="{{ htmlentities(Input::old("email"), ENT_QUOTES) }}" title="">
				@if($errors->has("email"))
				{{ $errors->first("email") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-sm-2 control-label">Password:</label>
			<div class="col-sm-10">
				<input type="password" name="password" id="inputPassword" class="form-control" value="" title="">
				@if($errors->has("password"))
				{{ $errors->first("password") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-sm-2 control-label">Password confirmation:</label>
			<div class="col-sm-10">
				<input type="password" name="password_confirmation" id="inputPassword" class="form-control" value="" title="">
				@if($errors->has("password_confirmation"))
				{{ $errors->first("password_confirmation") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
			<center>{{ Session::get('message') }}</center><br>
			<center><button type="submit" class="btn btn-primary">Register</button></center>
		</div>
	</form>
@stop
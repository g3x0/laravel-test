<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield("title")</title>
		<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
		<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<script src="{{ URL::asset("js/delete.js") }}"></script>
		<!--[if lt IE 9]>
		<script src="https:http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https:http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container">
			@include("layout.nav")
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">@yield("paneltitle")</h3>
				</div>
				<div class="panel-body">
					@yield("content")
				</div>
				<div class="panel-footer">
					<center>Copyright @ askwrite 2015</center>
				</div>
			</div>
		</div>
	</body>
</html>
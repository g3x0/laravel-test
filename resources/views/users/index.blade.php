@extends("layout.main")
@section("title") Utilizatori @stop
@section("navli")
	<li class="active"><a href="{{ URL::route("users") }}">Utilizatori</a></li>
	<li><a href="/auth/logout">Logout</a></li>
@stop
@section("paneltitle") Utilizatori @stop
@section("content")
	@if (!empty($users))
		<table class="table">
			<thead>
		        <tr>
		            <th>ID</th>
		            <th>Nume</th>
		            <th>E-Mail</th>
		        </tr>
		    </thead>
		    <tbody>
		@foreach ($users as $user)
			<tr>
	            <td>{{ htmlentities($user->id, ENT_QUOTES) }}</td>
	            <td>{{ htmlentities($user->name, ENT_QUOTES) }}</td>
	            <td>{{ htmlentities($user->email, ENT_QUOTES) }}</td>
	            <td><a class="btn btn-primary btn-sm" href="/utilizatori/edit/{{ $user->id }}">Edit</a></td>
	            <td><a class="btn btn-danger btn-sm" onclick="asd({{ $user->id }});">Delete</a></td>
	            <td id="{{ $user->id }}"></td>
	        </tr>
		@endforeach
	@else
		<br><br><center>Nu exista useri inregistrati ta.</center>
	@endif
			</tbody>
		</table>
@stop

@extends("layout.main")
@section("title") Edit user @stop
@section("navli")
	<li class="active"><a href="{{ URL::route("users") }}">Utilizatori</a></li>
	<li><a href="/auth/logout">Logout</a></li>
@stop
@section("paneltitle") User @stop
@section("content")

	<form class="form" action="/utilizatori/edit/{{ $user[0]->id }}" method="post" id="editForm">
		<div class="form-group">
			<label for="inputUsername" class="col-xs-7 control-label">Nume:</label>
			<div class="col-xs-12">
				<input type="text" name="Nume" id="inputUsername" class="form-control" value="{{ htmlentities($user[0]->name) }}" title="">
				@if($errors->has("Nume"))
				{{ $errors->first("Nume") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail" class="col-xs-7 control-label">Email:</label>
			<div class="col-xs-12">
				<input type="text" name="Email" id="inputEmail" class="form-control" value="{{ htmlentities($user[0]->email) }}" title="" disabled>
				@if($errors->has("Email"))
				{{ $errors->first("Email") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputNewPassword" class="col-xs-7 control-label">Parola noua:</label>
			<div class="col-xs-12">
				<input type="password" name="NewPassword" id="inputNewPassword" class="form-control" title="">
				@if($errors->has("NewPassword"))
				{{ $errors->first("NewPassword") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<label for="inputRepeatNewPassword" class="col-xs-7 control-label">Rescrie parola noua:</label>
			<div class="col-xs-12">
				<input type="password" name="RepeatNewPassword" id="inputRepeatNewPassword" class="form-control" title="">
				@if($errors->has("RepeatNewPassword"))
				{{ $errors->first("RepeatNewPassword") }}
				@endif
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<br>
				<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
				<center>{{ Session::get('message') }}</center><br>
				<center><button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Salveaza</button>
				<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button></center>
			</div>
		</div>
	</form>
@stop